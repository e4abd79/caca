const fs = require("fs");

let user = process.argv[2];
let file = "public-commands.json"
let body = fs.readFileSync(file, 'utf-8')
let start = Date.now();

/***************************/
/* BEGIN COPY FOR NIGHTBOT */
/***************************/

// assumes the existence of:
//   user: user to ping
//   body: json raw text

// Fossabot's !pinguser:
// https://decapi.me/twitch/random_user/saca__?exclude=kurosaki421,vraix,ariaanabatmen,lolwutpotat0
// Note: exclusion list may have changed

let r = (_ => {
    let commands = JSON.parse(body).commands;
    let n = commands.length;
    var locations;

    let bsearch = query => {
        let l = 0, r = n - 1;

        while (l <= r) {
            let m = Math.floor((l + r) / 2);
            let m_item = commands[m].name;

            if (m_item < query) {
                l = m + 1;
            } else if (m_item > query) {
                r = m - 1;
            } else {
                return m;
            }
        }

        return -1;
    };

    let idx = bsearch("locationsraw");
    if (idx >= 0) {
        locations = commands[idx].response.split(/, ?/);
    } else {
        return `"locationsraw" not found.`;
    }

    let rndidx = Math.floor(Math.random() * locations.length);
    let loc = locations[rndidx];

    idx = bsearch(loc);
    if (idx >= 0) {
        return `${user} is on vacation in/at the ${loc} ${commands[idx].response}`;
    } else {
        return `"${loc}" not found.`
    }
})();
r;

/*************************/
/* END COPY FOR NIGHTBOT */
/*************************/

console.log(r);
console.log(`${Date.now() - start} ms`)
