const fs = require("fs");

let file = "public-commands.json";
let body = fs.readFileSync(file, 'utf-8');

/***************************/
/* BEGIN COPY FOR NIGHTBOT */
/***************************/

// assumes the existence of:
//   body: json raw text

let st = Date.now();

// doesn't handle null
let isdigit = s => "0" <= s && s <= "9";

class Trie {
    constructor(value) {
        this.v = value;
        this.c = {}; // children
        this.n = 1; // number of occurrences
        this.e = false; // end of a word
    }

    insert(str) {
        let cur = this;
        cur.n += 1;

        for (let i = 0; i < str.length; i++) {
            let k = str[i];
            let v = cur.c[k];

            if (v) {
                cur = v
                cur.n += 1;
            } else {
                cur = cur.c[k] = new Trie(k);
            }
        }

        cur.e = true;
    }

    getAll(stack, list) {
        if (stack.length > 2 && isdigit(Object.keys(this.c)[0])) {
            let u = stack.join('');
            if (u !== "hasan") {
                list.push([this.n, u])
            }
            return;
        }

        for (let k in this.c) {
            stack.push(k);
            this.c[k].getAll(stack, list);
            stack.pop();
        }

        return list;
    }
};

let t = new Trie();
JSON.parse(body).commands.forEach(c => t.insert(c.name));
let f = (() => {
    // putting this block in a func allows the minifier to shorten var names

    let list = t.getAll([], []).sort((a, b) => b[0] - a[0]);
    let out = "";
    let last_rank = -1;
    let last_count = -1;
    for (let i = 0; i < 10; i++) {
        let count = list[i][0];
        let rank = count == last_count ? last_rank : i + 1;
        // Padding uses fake spaces: \u2800
        // The last space is a normal one, which invokes a line break.
        out += `${rank}.⠀${count}⠀-⠀${list[i][1]}`.padEnd(30, "⠀") + " ";
        last_count = count;
        last_rank = rank;
    }
    return out + `[${Date.now() - st}ms]`;
})();
f;

/*************************/
/* END COPY FOR NIGHTBOT */
/*************************/

console.log(f);
