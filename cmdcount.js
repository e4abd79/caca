const fs = require("fs");

let q = process.argv[2]
let file = "public-commands.json";
let body = fs.readFileSync(file, 'utf-8');

/***************************/
/* BEGIN COPY FOR NIGHTBOT */
/***************************/

// assumes the existence of:
//   q: query string
//   body: json raw text

let st = Date.now();

class Trie {
    constructor(value) {
        this.v = value;
        this.c = {}; // children
        this.n = 1; // number of occurrences
        this.e = false; // end of a word
    }

    insert(str) {
        let cur = this;

        for (let i = 0; i < str.length; i++) {
            let k = str[i];
            let v = cur.c[k];

            if (v) {
                cur = v
                cur.n += 1;
            } else {
                cur = cur.c[k] = new Trie(k);
            }
        }

        cur.e = true;
    }

    query(str) {
        str = str.trim().toLowerCase();
        let cur = this;

        let match = [];
        let i = 0;
        for (; i < str.length; i++) {
            let k = str[i];
            let v = cur.c[k];

            if (v) {
                match.push(k);
                cur = v;
            } else {
                break;
            }
        }

        let list = i == 0 ? 'modCheck' : cur.getAll([], []).sort((a, b) => {
            let a_is_number = typeof (a) == "number"
            let b_is_number = typeof (b) == "number"

            if (a_is_number && b_is_number) {
                return a - b;
            } else if (!a_is_number && !b_is_number) {
                return a < b;
            } else {
                return a_is_number ? -1 : 1;
            }
        });

        if (typeof (list) != "string") {
            // consolidate consecutive numbers to reduce output

            // add a dummy element in case all elements are numbers
            list.push(NaN);

            let i = 0;
            while (i < list.length && !isNaN(list[i])) {
                let j = i + 1;
                while (j < list.length && list[j] - 1 == list[j - 1]) {
                    j++;
                }

                if (j - i > 1){
                    list.splice(i, j - i, `${list[i]}-${list[j - 1]}`)
                }
                i++;
            }

            list.pop()
        }

        return [match.join(''), cur.n, list];
    }

    getAll(stack, list) {
        if (this.e) {
            let s = stack.join('');
            if (/^\d+$/.test(s)) {
                list.push(parseInt(s));
            } else {
                list.push(stack.join(''));
            }
        }

        for (let k in this.c) {
            stack.push(k);
            this.c[k].getAll(stack, list);
            stack.pop();
        }

        return list;
    }
};

let r = (_ => {
    let t = new Trie();
    let commands = JSON.parse(body).commands;
    let s;

    if (q) {
        // Only add the command into the trie if the first letter matches.
        // This should help execution time when running in the bot.
        commands.forEach(cmd => cmd.name[0] === q[0] ? t.insert(cmd.name) : 0);
        let r = t.query(q.replace(/@/, ''));
        s = `${r[0]} count: ${r[1]} { ${r[2]} } [${Date.now() - st} ms, matching prefix]`;
    } else {
        s = commands.length;
    }
    return s;
})();
r;

/*************************/
/* END COPY FOR NIGHTBOT */
/*************************/

console.log(r)
